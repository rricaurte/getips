# README #

### What is this repository for? ###

* __Quick summary__: From a given stream of text--extracted from the
  clipboard--search for unique IP addresses. Once found, they are returned to
  the clipboard ready to be pasted.
* __Version__: 0.5.

### How do I get set up? ###

* Dependencies
    * Python >= 3.6
    * pyperclip module
        * `pip3 install pyperclip --user`

### Contribution guidelines ###

* Select the text that should contain the IP addresses.
* Copy the text to the clipboard. \(Ctrl + C\)
* Execute the script in any of these ways:
    * `:-$ python3 /path/to/getips.py`, or
    * `:-$ ./getips.py` \(Execution privileges had to be granted\)
* Paste the extracted and formatted IP addresses where they are needed.
 \(Ctrl + V\)

### Who do I talk to? ###

* Repo owner or admin.
