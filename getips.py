#!/usr/bin/env python3
"""getips

From a given stream of text--extracted from the clipboard--search
for unique IP addresses. Once found, they are returned to the
clipboard ready to be pasted.
"""

__author__ = 'Ruben Ricaurte <ricaurtef@gmail.com>'


import re
import sys
from argparse import ArgumentParser

import pyperclip


def args():
    """Handle command-line postiional arguments and options."""
    parser = ArgumentParser(
        prog='getips',
        description='Extracts IP addresses from the clipboard, and returns \
                     them back to it as a comma-separated list ready to be \
                     pasted.',
    )
    parser.add_argument(
        '-a',
        dest='alt',
        action='store_const',
        const=' ',
        default=', ',
        help='alternative output (no commas)',
    )

    return parser.parse_args()


def get_ip_addresses(text):
    """Extract IPs from a string."""
    ip_regex = re.compile(r'''
        (?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}   # First 3 octets.
        (?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)            # Last octet.
    ''', re.VERBOSE)

    return ip_regex.finditer(text)


def main():
    """Entry point."""
    text = pyperclip.paste()
    argv = args()

    if text:
        blacklist = {mo.group() for mo in get_ip_addresses(text)}
        if blacklist:
            sep = argv.alt
            pyperclip.copy(sep.join(blacklist))  # Copy found to clipboard.

            # Some human-friendly output.
            print('The following was added to the clipboard:')
            print('\n'.join(blacklist))
        else:
            print('No IP addresses found.')
    else:
        print('There is nothing to copy from the clipboard.')
        sys.exit(1)


if __name__ == '__main__':
    main()
